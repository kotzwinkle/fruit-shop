﻿using FruitShop.Web.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace FruitShop.Web.Repositories.Fruit
{
    public class DummyFruitRepository : IFruitRepository
    {
        private const decimal MaximumDecimalValue = 10m;
        private const int GenerationAmount = 50;
        private Random random = new Random();

        public IEnumerable<Models.Fruit> GetAll()
        {
            var models = new List<Models.Fruit>();
            for (int i = 0; i < GenerationAmount; i++)
            {
                var model = GetRandomFruit();
                model.Id = Guid.NewGuid();
                model.Weight = GetRandomDecimal();
                var color = GetRandomColor();
                model.Color = ColorTranslator.ToHtml(color);
                model.Price = GetRandomDecimal();
                model.Picked = GetRandomDateTime();
                model.Seeds = GetRandomBoolean();
                models.Add(model);
            }
            return models;
        }


        public Models.Fruit GetRandomFruit() {
            var value = random.Next(6);
            switch (value)
            {
                case 0:
                    return new Apple();
                case 1:
                    return new Banana();
                case 2:
                    return new Cherry();
                case 3:
                    return new Coconut();
                case 4:
                    return new Kiwi();
                default:
                    return new Strawberry();

            }
        }
        public Color GetRandomColor() { 
            var red = random.Next(256);
            var blue = random.Next(256);
            var green = random.Next(256);
            var color = Color.FromArgb(red, green, blue);
            return color;
        }
        public decimal GetRandomDecimal() {
            var randomValue = random.NextDouble();
            var amount = (decimal)randomValue * MaximumDecimalValue;
            return amount;
        }

        public bool GetRandomBoolean() {
            var value = random.NextDouble() >= 0.5;
            return value;
        }

        public DateTime GetRandomDateTime()
        {
            var endDate = DateTime.Now;
            var startDate = endDate.AddMonths(-1);
            var timeSpan = endDate - startDate;
            var newSpan = new TimeSpan(0, random.Next(0, (int)timeSpan.TotalMinutes), 0);
            var newDate = startDate + newSpan;
            return newDate;
        }
    }
}