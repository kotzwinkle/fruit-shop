﻿using FruitShop.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FruitShop.Web.Repositories.Fruit
{
    public interface IFruitRepository
    {
        IEnumerable<Models.Fruit> GetAll();
    }
}
