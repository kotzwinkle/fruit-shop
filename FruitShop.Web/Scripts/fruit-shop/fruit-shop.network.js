﻿var fruitShop = fruitShop || {};
fruitShop.library = fruitShop.library || {};
(function (scope) {
    scope.network = {};
    (function (scope) {
        scope.method = {
            get: "GET"
        };
        scope.sendRequest = function (url, type, payload, successContext, successCallback, failureContext, failureCallback) {
            let data = null;
            if (payload != null) {
                data = JSON.stringify(data);
            }
            $.ajax({
                url: url,
                type: type,
                data: data,
                contentType: "application/json",
                cache: false
            }).done(function (response) {
                if (successContext) {
                    successCallback.call(successContext, response);
                    return;
                }
                successCallback(response);
            }).fail(function (xhr, status, error) {
                if (failureContext) {
                    failureCallback.call(failureContext, xhr, status, error);
                    return;
                }
                failureCallback(xhr, status, error);
            });
        };
    })(scope.network);
})(fruitShop.library);