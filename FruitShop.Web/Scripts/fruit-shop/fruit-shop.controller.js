﻿var fruitShop = fruitShop || {};
(function (scope) {
    scope.controller = {};
    (function (scope, network) {
        scope.fruitController = function (configuration) {
            this.configuration = configuration;
        };
        scope.fruitController.prototype.getFruits = function (successContext, successCallback, failureContext, failureCallback) {
            let url = this.configuration.baseUrl + this.configuration.getFruitsRoute;
            network.sendRequest(url, network.method.get, null, successContext, successCallback, failureContext, failureCallback);
        };
    })(scope.controller, scope.library.network);

})(fruitShop);