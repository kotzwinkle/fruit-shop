﻿
var fruitShop = fruitShop || {};
(function (scope) {
    scope.model = {};
    (function (scope) {
        scope.fruit = function () {
            this.id = null;
            this.type = null;
            this.weight = null;
            this.color = null;
            this.price = null;
            this.picked = null;
            this.seeds = null;
        };

        scope.fruit.instantiate = function (model) {
            let fruit = new scope.fruit();
            fruit.id = model.Id;
            fruit.type = model.Type;
            fruit.weight = model.Weight;
            fruit.color = model.Color;
            fruit.price = model.Price;
            fruit.picked = new Date(model.Picked);
            fruit.seeds = model.Seeds;
            return fruit
        }
    })(scope.model);
})(fruitShop);