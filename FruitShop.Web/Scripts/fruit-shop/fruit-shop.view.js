﻿var fruitShop = fruitShop || {};
(function (scope) {
    scope.view = {};
    (function (scope, controller, model) {
        scope.main = function (configuration) {
            let self = this;
            this.configuration = configuration;
            this.fruitController = new controller.fruitController(configuration);
            this.currentFruits = [];
            this.fruitTable = $(`#${configuration.element.fruitTableId}`);
            this.dateFilter = $(`#${configuration.element.dateFilterId}`);
            this.filterToggle = $(`#${configuration.element.filterToggleId}`);
            this.calloutContainer = $(`#${configuration.element.calloutContainerId}`);
            this.calloutTemplateScript = $(`#${configuration.element.calloutTemplateId}`);
            this.calloutTemplate = Handlebars.compile(this.calloutTemplateScript.html());
            this.filterToggle.change(function () {
                let enabled = $(this).is(":checked");
                self.setDateFilterEnabled(enabled);
            });

            this.dateFilter.change(function () {
                let enabled = self.filterToggle.is(":checked");
                if (!enabled) {
                    this.dateFilter.removeClass("is-invalid");
                    return;
                }

                self.setDateFilterEnabled(enabled);
            })

            this.fruitTable.DataTable({
                rowId: 'id',
                columns: [
                    { title: 'Id', data: 'id' },
                    { title: 'Type', data: 'type' },
                    { title: 'Weight', data: 'weight' },
                    {
                        title: 'Color', data: 'color',
                        render: function (data, type, row) {
                            return `<span>${data} <i class="fa fa-square" style="color:${data};"></i><span>`;
                        }
                    },
                    { title: 'Price', data: 'price' },
                    {
                        title: 'Picked', data: 'picked',
                        render: function (data, type, row) {
                            return moment(data).format('YYYY-MM-DD');
                        }
                    },
                    { title: 'Seeds', data: 'seeds' },
                ]
            });
        };
        scope.main.prototype.start = function () {
            this.retrieveFruits();
        };

        scope.main.prototype.setDateFilterEnabled = function (value) {
            this.clearFruitTable();
            if (!value) {
                this.populateFruitTable(this.currentFruits);
                this.dateFilter.removeClass("is-invalid");
                return;
            }

            let dateValue = this.dateFilter.val();
            if (dateValue.trim().length == 0) {
                this.dateFilter.addClass("is-invalid");
                this.populateFruitTable(this.currentFruits);
                return;
            }

            let comparisonDate = new Date(dateValue)
            let filteredFruits = _.filter(this.currentFruits, function (fruit) { return fruit.picked > comparisonDate });
            this.populateFruitTable(filteredFruits);
        };

        scope.main.prototype.retrieveFruits = function () {
           // this.calloutContainer.hide();
            this.fruitController.getFruits(this, this.onFruitsRetrieved, this, this.showError);
        };

        scope.main.prototype.clearFruitTable = function () {
            this.fruitTable.DataTable().clear().draw();
        };

        scope.main.prototype.onFruitsRetrieved = function (fruits) {

            var convertedFruits = [];
            for (let i = 0; i < fruits.length; i++) {
                let fruit = model.fruit.instantiate(fruits[i]);
                convertedFruits.push(fruit);
            } 

            this.currentFruits = convertedFruits;
            this.populateFruitTable(convertedFruits);
        };

        scope.main.prototype.populateFruitTable = function (fruits) {
            this.fruitTable.DataTable().clear().rows.add(fruits).draw();
        };

        scope.main.prototype.showError = function (xhr, status, error) {
            this.updateCallout("Error", "Error encountered when retrieving fruits");
            //this.calloutContainer.show();
        };

        scope.main.prototype.updateCallout = function (header, message) {
            let model = {
                header: header,
                message: message
            };
            let html = this.calloutTemplate(model)
            this.calloutContainer.html(html);
        };


    })(scope.view, scope.controller, scope.model);

})(fruitShop);