﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FruitShop.Web.Models
{
    public abstract class Fruit
    {
        public Guid Id { get; set; }
   
        public decimal Weight { get; set; }
        public string Color { get; set; }
        public decimal Price { get; set; }
        public DateTime Picked { get; set; }
        public bool Seeds { get; set; }
        public void SetEdible(bool value) { 
            //does stuff
        }

        public abstract string Type { get; }
    }
}