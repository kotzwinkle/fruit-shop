﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FruitShop.Web.Models
{
    public class Banana : Fruit
    {
        public override string Type => "Banana";
    }
}