﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FruitShop.Web.Models
{
    public class Coconut : Fruit
    {
        public override string Type => "Coconut";
    }
}